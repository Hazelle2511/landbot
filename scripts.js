var width = 500; // viewbox sets fixed width/height
var height = 500;
var margin = {
    top: 20,
    right: 20,
    bottom: 20,
    left: 20
};

// Calculate some sizes
var maxRadius = Math.min(
    (width - margin.left - margin.right) / 2,
    (height - margin.top - margin.bottom) / 2
);
var minLevelRadius = maxRadius * .2; // At 20% of max radius
var maxLevelRadius = maxRadius * .75; // At 75% of max radius
var groupRadius = maxRadius * .9; // At 90% of max radius
var backgroundRadius = maxLevelRadius * 1.05; // Extend a bit outside level radius
var textPosition = maxLevelRadius * .8; // Extend a bit outside level radius
var groupPaddingAngle = 0.02;

// Global variables and functions used within template (values will be assigned later)
var partWidthInDegrees = 0;
var partWidthInRadians = 0;
var families = {};
var radiusScale = null;
var wedgeArcGenerator = null;
var groupTextArcGenerator = null;

function groups(d) {

    // Create an array of groups. Every group consists of an id, the name of the
    // group, the number of objectives within the group and the start (index)
    // of the group. The count specifies the size of the arc shown 'above' the group.
    // The start index specifies where the arc should start.
    // The id is used to reference the arc (to allow the text to be put onto the arc
    // of the group).
    return d.reduce(function(groups, objective, i) {

        // Check if current objective has same group as last time
        if (groups.length > 0 && groups[groups.length - 1].name === objective.group) {

            // Increase group counter
            groups[groups.length - 1].count++;
        } else {
            // Add new group
            groups.push({
                id: 'group' + groups.length,
                name: objective.group,
                count: 1,
                start: i
            });
        }

        return groups;
    }, []);
}

function groupArc(d) {

    // Create path data for group arc
    // HACK: Replace everything from first straight line onward. This leaves the
    // single arc 'curve' as data. This hack assumes some knowledge about the
    // way the data is generated and might therefore fail if the arc generator
    // is implemented differently.
    // Alternative would be to do the math here directly.
    var arcData = groupTextArcGenerator({
        startAngle: d.start * partWidthInRadians,
        endAngle: (d.start + d.count) * partWidthInRadians
    });

    // Replace all line (and other) commands
    arcData = arcData
        .replace(/[Ll][^Aa]*/, "");
    return arcData;
}

//for the rate bar
function wedgeArc(d) {
    return wedgeArcGenerator({
        innerRadius: radiusScale(d),
        outerRadius: radiusScale(d) + radiusScale.bandwidth()
    });
}
var count = 0;
// Read in data and render onto template
function buildChart(error, objectives) {

    // Convert/extract objectives data
    var familyColorIndex = 1;
    objectives.forEach(function(objective) {

        // Convert string to integer
        objective.rate = parseInt(objective.rate);

        // Give every family unique color index
        if (!families[objective.family]) {
            families[objective.family] = familyColorIndex;
            familyColorIndex++;
        }
    });



    // Create scale for radius wedges (donut parts)
    // See band scales: https://github.com/d3/d3-scale#band-scales
    // extent method return min and max of an array
    // @see https://github.com/d3/d3-array/blob/v2.12.0/README.md#extent
    var difficultyExtent = d3.extent(objectives, function(objective) {
        return objective.rate;
    });

    // Get ratio between svg radius and values
    var difficultyRange = d3.range(difficultyExtent[0], difficultyExtent[1] + 1); // + 1 since 'stop' is not in range
    radiusScale = d3.scaleBand()
        .domain(difficultyRange)
        .range([minLevelRadius, maxLevelRadius])
        /*It gives space for the rate scale*/
        // .paddingInner(.2);

    // Create arc generator for wedges (donut parts)
    // See arcs: https://github.com/d3/d3-shape#arcs
    partWidthInDegrees = 360 / objectives.length;
    partWidthInRadians = 2 * Math.PI / objectives.length;
    wedgeArcGenerator = d3.arc()
        .startAngle(0)
        .endAngle(partWidthInRadians);

    // Create arc generator for group text
    groupTextArcGenerator = d3.arc()
        .innerRadius(0) // This will in fact create pie piece (see "groupArc" function above for further explanation)
        .outerRadius(groupRadius)
        .padAngle(groupPaddingAngle);

    // Create template and render data onto template
    if (!count++)
        d3.select("svg").template().render(objectives);
    else
        d3.select("svg").render(objectives);



}




d3.csv("objectives.csv", buildChart);