## Value Wheel, Premiére Version - Creé par -
 Hazelle Camacho
 Marie Guerin
 Shivansh Tyagi

## Landbot - https://landbot.io/

## Brief Value Wheel Slides 
https://docs.google.com/presentation/d/1Aq9oF1nwxRPejJbsZKOhdSxvsoQCdOyP0-lMDOFM79g/edit?ts=6045f8c5#slide=id.gc2583b3923_0_0

## Textes landbot value wheel 
https://docs.google.com/document/d/1gWPAbO36rU96rvemYiZNQnSb8ovj_zxajtj4MrTqOms/edit?ts=6049de82

## Charts Diagramme -- Bibliothéque JS utilisé - 
Data-Driven Documents - https://d3js.org/
UNPKG - https://unpkg.com/browse/d3@3.5.9/

## Procedure in Landbot/ DEMO
https://docs.google.com/document/d/1zOl9_dV7bpqi4fdBk2AA6VAz4wN2ywyKue3VGqGcJy4/edit?usp=sharing


<!-- If the diagram displays only the logo, please refresh the page -->